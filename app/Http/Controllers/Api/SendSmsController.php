<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class SendSmsController extends Controller
{
    public function send_sms(Request $request)
    {
        $validate = Validator::make($request->all(),
            [
                'to'       => 'digits:10',
                'content'  => 'required|max:160',
                'provider' => 'required|in:KALEYRA,MSG91',
            ]);
        if ($validate->fails()) {
            return response()->json(array('message' => $validate->errors()->first()), 422);
        }
        $to       = $request->to;
        $content  = $request->content;
        $provider = $request->provider;
        if ($provider === 'KALEYRA') {
            $response = self::Kaleyra($to, $content);
        } elseif ($provider === 'MSG91') {
            $response = self::msg91($to, $content);
        }

        return response()->json(array('message' => $response['message']), 200);
    }

    public static function Kaleyra($to, $content)
    {
        $content = rawurlencode($content);
        ## & makes problem in query string thats why remove & by contents by and
        $content = str_replace("&", "and", $content);
        $api_key = 'A892b88d0ac1c5322d9093a55815b767om';
        ## i will take rand A892b88d0ac1c5322d9093a55815b767om api key it is not real
        $url = 'https://api-alerts.kaleyra.com/v4/?api_key=' . $api_key . '&method=sms&message=' . $content . '&to=' . $to . '&sender=Test';
        $crl = curl_init();
        curl_setopt($crl, CURLOPT_URL, $url);
        curl_setopt($crl, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($crl);
        if (!$response) {
            die('Error: "' . curl_error($crl) . '" - Code: ' . curl_errno($crl));
        }
        curl_close($crl);
        return json_decode($response, 1)
        ;

    }
    public static function msg91($to, $content)
    {
        $curl               = curl_init();
        $authentication_key = "A892b88d0ac1c5322d9093a55815b767om";
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.msg91.com/api/v2/sendsms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => "{ \"sender\": \"TEST\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"$content\", \"to\": [ \"$to\"] } ] }",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER     => array(
                "authkey: $authentication_key",
                "content-type: application/json",
            ),
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);
        return json_decode($response, 1);

    }
}
